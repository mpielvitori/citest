# Gitlab pipeline test project with semantic release

### Using push rule with notes workaround -> [issue](https://github.com/semantic-release/semantic-release/issues/1446)
`(^(Notes added by \'git notes add\'))|(^(feat|fix|docs|breaking|config|no-release|style|refactor|test|build|ci|perf|chore)(\(.+\))?\:\s(.{3,}))`

### Running commitlint on pipeline
```
  - npm i -g @commitlint/cli @commitlint/config-conventional
  - npx commitlint --from=$CI_BUILD_BEFORE_SHA
```

### Running commitlint on pipeline only last commit
git log -1 --pretty=%B -> raw body (unwrapped subject and body)
git log -1 --pretty=%s -> subject
```
  - npm i -g @commitlint/cli @commitlint/config-conventional
  - echo $(git log -1 --pretty=%B) | commitlint
```
