#!/bin/sh

echo "testing"

test "" = "$(egrep '^(build|ci|docs|feat|fix|perf|refactor|style|test|chore)(\([a-z]+\)){0,1}:\s.+' "$1")" && {
   cat $1
   exit 1
}

exit 0
