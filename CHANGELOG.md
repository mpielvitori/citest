## [14.1.3](https://gitlab.com/mpielvitori/citest/compare/v14.1.2...v14.1.3) (2020-08-10)


### Bug Fixes

* validate unwrapped body ([f8e6563](https://gitlab.com/mpielvitori/citest/commit/f8e6563b01abfbf88a9e013cb15bba852eb2bd27))

## [14.1.2](https://gitlab.com/mpielvitori/citest/compare/v14.1.1...v14.1.2) (2020-08-10)


### Bug Fixes

* update readme ([d1f0415](https://gitlab.com/mpielvitori/citest/commit/d1f0415bc9ea2879dc82c092a89bed580370c182))
* validate last commit subject ([b0fb025](https://gitlab.com/mpielvitori/citest/commit/b0fb0259bc25f40d7516786920318f01d76ebc83))

## [14.1.1](https://gitlab.com/mpielvitori/citest/compare/v14.1.0...v14.1.1) (2020-08-10)


### Bug Fixes

* fail ([46771e6](https://gitlab.com/mpielvitori/citest/commit/46771e6a7c103518dc9fb8378340dc8866f2945a))

# [14.1.0](https://gitlab.com/mpielvitori/citest/compare/v14.0.0...v14.1.0) (2020-08-10)


### Bug Fixes

* generate version ([19b5b6d](https://gitlab.com/mpielvitori/citest/commit/19b5b6d03d2fb7086e7ce3e4f7e5b90bcd74c1d5))
* use commitlint directly ([ada9e80](https://gitlab.com/mpielvitori/citest/commit/ada9e80489b8f6087694fbc7cdeae0918dbce7df))


* lint only the last commit ([853cab7](https://gitlab.com/mpielvitori/citest/commit/853cab7164bd9de7ff5dae5a4a22fcffa46a4ef9))
* mycommit ([3626324](https://gitlab.com/mpielvitori/citest/commit/36263246d180ddd210823f4fb084abcc195d2049))
* pepa ([39dde76](https://gitlab.com/mpielvitori/citest/commit/39dde76a0ac642012734a525f7fba12e0937530b))


### Features

* update readme ([b58eb1d](https://gitlab.com/mpielvitori/citest/commit/b58eb1d538f122201978b627702f62bf81718ec0))

# [14.0.0](https://gitlab.com/mpielvitori/citest/compare/v13.0.0...v14.0.0) (2020-08-10)


### Breaking

* lala4 ([d69d83b](https://gitlab.com/mpielvitori/citest/commit/d69d83bd7c3e955b4de036016dbf088698fadbbb))

# [13.0.0](https://gitlab.com/mpielvitori/citest/compare/v12.3.2...v13.0.0) (2020-08-10)


### Breaking

* lala ([df579cf](https://gitlab.com/mpielvitori/citest/commit/df579cf9e757ab123f6ad44c9864334f9183ecf0))
* lala2 ([c3767bb](https://gitlab.com/mpielvitori/citest/commit/c3767bbd9933db27c829c43f02f2b7cde4cac97c))
* lala3 ([daa453d](https://gitlab.com/mpielvitori/citest/commit/daa453df8a410a4dd68206ab07b9ef2f32dbb3d2))


* test6 ([12be2c8](https://gitlab.com/mpielvitori/citest/commit/12be2c8dc48102667e52683843b63a083dc71f5d))
* test5 ([8b323bb](https://gitlab.com/mpielvitori/citest/commit/8b323bb162ce05679dea66bdeeb9806f3b2d2a4e))
* test4 ([3c8ed9d](https://gitlab.com/mpielvitori/citest/commit/3c8ed9d98ef828912c64c776538d432446a82fd7))
* test3 ([778c28c](https://gitlab.com/mpielvitori/citest/commit/778c28c0ceb2e279dadc4aa18efcae1c2a44f708))
* test2 ([c3ed44e](https://gitlab.com/mpielvitori/citest/commit/c3ed44e6427b9dbaa3b887cd43ad503adf27ee03))
* test ([e1877e2](https://gitlab.com/mpielvitori/citest/commit/e1877e2d011e897bf685a43775f08538c3ff4792))

## [12.3.2](https://gitlab.com/mpielvitori/citest/compare/v12.3.1...v12.3.2) (2020-08-08)


### Bug Fixes

* test commit ([ca680b0](https://gitlab.com/mpielvitori/citest/commit/ca680b0e7c94ceec6c6ee5e8c272e1badd14d9fb))

## [12.3.1](https://gitlab.com/mpielvitori/citest/compare/v12.3.0...v12.3.1) (2020-08-08)


### Bug Fixes

* add commit-msg ([7532191](https://gitlab.com/mpielvitori/citest/commit/753219180c72d61764df9f1c33e973c70ad4ba6d))

# [12.3.0](https://gitlab.com/mpielvitori/citest/compare/v12.2.2...v12.3.0) (2020-08-08)


### Features

* use push rule ([9793e4e](https://gitlab.com/mpielvitori/citest/commit/9793e4e50aa7c36f0aae1d4fd0abbff566087ae0))

## [12.2.2](https://gitlab.com/mpielvitori/citest/compare/v12.2.1...v12.2.2) (2020-08-08)


### Bug Fixes

* add changelog commit ([8864af7](https://gitlab.com/mpielvitori/citest/commit/8864af7df21efd8fb0b667e4f66fa7446bb8571d))

# [12.2.0](https://gitlab.com/mpielvitori/citest/compare/v12.1.0...v12.2.0) (2020-08-08)


### Features

* push rule regex ([1284b15](https://gitlab.com/mpielvitori/citest/commit/1284b15ce255fbd7691748da252936f15daa85e7))

# [12.1.0](https://gitlab.com/mpielvitori/citest/compare/v12.0.0...v12.1.0) (2020-08-08)


### Features

* test push validation rule ([4d58d34](https://gitlab.com/mpielvitori/citest/commit/4d58d341ba416ee48cda3b85a435329d40860466))


* Update README.md ([e16d3e0](https://gitlab.com/mpielvitori/citest/commit/e16d3e0844fcd31ea2e3d2fb6994fda451e76d42))


### revert

* new version ([14cb9a6](https://gitlab.com/mpielvitori/citest/commit/14cb9a6e545f7053551b94dcce898cae03c25908))

# [12.0.0](https://gitlab.com/mpielvitori/citest/compare/v11.8.0...v12.0.0) (2020-04-30)


### Breaking

* new version ([19321b6](https://gitlab.com/mpielvitori/citest/commit/19321b64ef49e02fc35777b9ea71ba1779550de0))

# [11.8.0](https://gitlab.com/mpielvitori/citest/compare/v11.7.2...v11.8.0) (2020-04-30)


### Bug Fixes

* pipeline ([4107b9a](https://gitlab.com/mpielvitori/citest/commit/4107b9a0fc6ed5b09494e44d00e6e334ca3f11a4))
* update readme ([3146c46](https://gitlab.com/mpielvitori/citest/commit/3146c46e2db5368695574efa22fb5ccb3fa82f93))


### Features

* fix ([5c1f4e4](https://gitlab.com/mpielvitori/citest/commit/5c1f4e49745d4924ce8ceec982cc1fad0ff3f9c8))


* Revert "feat(4): add changelog" ([d66081a](https://gitlab.com/mpielvitori/citest/commit/d66081a05fcd881d0d93d0d9f12bbed13f1808d9))
* Revert "Revert "fix(1): remove add apk git"" ([7b3987c](https://gitlab.com/mpielvitori/citest/commit/7b3987c31b98b8ba8902944984c03bcaa3675b53))

## [11.7.2](https://gitlab.com/mpielvitori/citest/compare/v11.7.1...v11.7.2) (2020-04-30)


### perf

* **011:** update readme ([3658e86](https://gitlab.com/mpielvitori/citest/commit/3658e8639e4ecfdedb333c0eb920d147eb09f701))

## [11.7.1](https://gitlab.com/mpielvitori/citest/compare/v11.7.0...v11.7.1) (2020-04-30)


* Revert "fix(1): remove add apk git" ([0009c48](https://gitlab.com/mpielvitori/citest/commit/0009c4861fdab2c0252c51c5ca5356a7296997f2))


### Bug Fixes

* **1:** remove add apk git ([55421db](https://gitlab.com/mpielvitori/citest/commit/55421dbc980407a2973d68ed843ad1ffe1132139))

# [11.7.0](https://gitlab.com/mpielvitori/citest/compare/v11.6.0...v11.7.0) (2020-04-27)


### Features

* **4:** add changelog ([0d9ec52](https://gitlab.com/mpielvitori/citest/commit/0d9ec520fe7445ebab744976aa0d982fbf798495))

# [11.6.0](https://gitlab.com/mpielvitori/citest/compare/v11.5.0...v11.6.0) (2020-04-27)


### Features

* **2:** my feat ([6e1eb45](https://gitlab.com/mpielvitori/citest/commit/6e1eb4543925eacb00696baf4c019b76628322b0))
